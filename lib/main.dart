
import 'package:flutter/material.dart';

import 'routes/locationPermission/location_permission_page.dart';
import 'routes/login/login_page.dart';
import 'routes/main/choose_hunt_page.dart';
import 'routes/main/main_page.dart';
import 'routes/main/my_home_page.dart';
import 'routes/realWorldHunt/real_hunt_detail_page.dart';
import 'routes/splashScreen/splash_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      title: 'Choohoo',
      routes: <String, WidgetBuilder>{
        // define the routes
        'LOCATION_PEMISSION_PAGE': (BuildContext context) => new LocationPermissionPage(),
        'MAIN_PAGE': (BuildContext context) => new MainPage(),
        'LOGIN_PAGE': (BuildContext context) => new LoginPage(),
        'CHOOSE_HUNT_PAGE': (BuildContext context) => new ChooseHuntPage(),
        'REAL_HUNT_PAGE': (BuildContext context) => new RealHuntDetailPage(),
        'HOME_PAGE':(BuildContext context) => new MyHomePage(),


      },
      home: SplashScreen(),
      color: Colors.black,
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  // MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _count = 0;
  String imageAddress = "assets/images/placeholder/black_adidas.png";

  int keyValue=0;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AnimatedSwitcher(
            duration: const Duration(seconds: 20),
            reverseDuration: const Duration(seconds: 20),
            transitionBuilder: (Widget child, Animation<double> animation) {
              return ScaleTransition(child: child, scale: animation);
            },
            child: Container(
              key: ValueKey<int>(_count),
              child: Image.asset(
                imageAddress,
              ),
            ),
          ),
          RaisedButton(
            child: const Text('Increment'),
            onPressed: () {
              setState(() {
                imageAddress== "assets/images/placeholder/red_adidas.png"?
                imageAddress = "assets/images/placeholder/black_adidas.png":
                imageAddress = "assets/images/placeholder/red_adidas.png";
              });
            },
          ),
        ],
      ),
    );
  }
}

