import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/receipts/receipt_details_clue_one.dart';
import 'package:world/routes/receipts/receipt_details_clue_two.dart';
import 'package:world/routes/receipts/receipt_details_riddle.dart';
import 'package:world/routes/receipts/receipt_details_units.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/app_utils.dart';

class ReceiptPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReceiptPageState();
  }
}

class ReceiptPageState extends State<ReceiptPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBarBuy(),
        body: SingleChildScrollView(
            child: Container(
          width: double.infinity,
          child: Container(
            child: Column(children: <Widget>[
              SizedBox(height: 20),
              AutoSizeText(
                Localization.stLocalized("receiptPage").toUpperCase(),
                style: CTheme.textRegular16DarkGrey(),
                textAlign: Localization.textAlignLeft(),
              ),
              _searchBar(),
              Padding(
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16, top: 10, bottom: 10),
                child: _listOfItems(),
              ),
              SizedBox(
                height: 10,
              )
            ]),
          ),
        )));
  }

  Widget _searchBar() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 15, 16, 5),
      child: Container(
        alignment: Alignment.center,
        height: 50,
        child: TextFormField(
          style: TextStyle(color: Colors.white),
          cursorColor: Colors.white,
          decoration: InputDecoration(
            suffixIcon: IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: () => {}),
            contentPadding: EdgeInsets.fromLTRB(18, 0, 18, 0),
            hintText: Localization.stLocalized("search"),
            hintStyle: TextStyle(color: Colors.white, fontSize: 16),
            filled: true,
            fillColor: Color.fromRGBO(70, 69, 69, 1),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.white,
                width: 1,
                style: BorderStyle.solid,
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                  width: 1,
                ),
                borderRadius: BorderRadius.all(const Radius.circular(20))),
          ),
        ),
      ),
    );
  }

  Widget _listOfItems() {
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        _unitContainer(),
        SizedBox(
          height: 10,
        ),
        _riddleContainer(),
        SizedBox(
          height: 10,
        ),
        _clueOneContainer(),
        SizedBox(
          height: 10,
        ),
        _clueTwoContainer(),
      ],
    );
  }

  _unitContainer() {
    return GestureDetector(
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, left: 15),
                  child: Text(
                    Localization.stLocalized("unitsPurchased"),
                    style: CTheme.textRegular14Black(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 15),
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.redAccent,
                          width: 1,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.redAccent,
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  padding:
                      EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  child: Image.asset(
                      "assets/images/triangle/triangle_units.png"),
                  decoration: AppUtils.circularBoxDecoration(
                      background: CColor.appWhite, border: CColor.appGreyLight)
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0, top: 10),
                    child: AutoSizeText(
                      Localization.stLocalized("codeMessage"),
                      style: CTheme.textLight14Black(),
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.start,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  AutoSizeText(
                    Localization.stLocalized("dateTime"),
                    style: CTheme.textLight11Black(),
                  ),
                  AutoSizeText(
                    Localization.stLocalized("recieptNumber"),
                    style: CTheme.textLight11Black(),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
            )
          ],
        ),
        decoration: AppUtils.circularBoxDecoration(
            background: CColor.appWhite, border: CColor.appGreyLight),
      ),
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ReceiptDetailsUnits()));
      },
    );
  }

  _riddleContainer() {
    return GestureDetector(
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, left: 15),
                  child: Text(
                    Localization.stLocalized("riddlePurchased"),
                    style: CTheme.textRegular14Black(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 15),
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.redAccent,
                          width: 1,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.redAccent,
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  padding:
                      EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  child: Image.asset(
                      "assets/images/triangle/triangle_riddle.png"),
                  decoration: AppUtils.circularBoxDecoration(
                      background: CColor.appWhite, border: CColor.appGreyLight)
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0, top: 10),
                    child: AutoSizeText(
                      Localization.stLocalized("codeMessage"),
                      style: CTheme.textLight14Black(),
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.start,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  AutoSizeText(
                    Localization.stLocalized("dateTime"),
                    style: CTheme.textLight11Black(),
                  ),
                  AutoSizeText(
                    Localization.stLocalized("recieptNumber"),
                    style: CTheme.textLight11Black(),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
            )
          ],
        ),
        decoration: AppUtils.circularBoxDecoration(
            background: CColor.appWhite, border: CColor.appGreyLight),
      ),
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ReceiptDetailsRiddle()));
      },
    );
  }

  _clueOneContainer() {
    return GestureDetector(
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, left: 15),
                  child: Text(
                    Localization.stLocalized("clueOnePurchased"),
                    style: CTheme.textRegular14Black(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 15),
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.redAccent,
                          width: 1,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.redAccent,
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  padding:
                      EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  child: Image.asset(
                      "assets/images/triangle/triangle_clue_one.png"),
                  decoration: AppUtils.circularBoxDecoration(
                      background: CColor.appWhite, border: CColor.appGreyLight),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0, top: 10),
                    child: AutoSizeText(
                      Localization.stLocalized("codeMessage"),
                      style: CTheme.textLight14Black(),
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.start,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  AutoSizeText(
                    Localization.stLocalized("dateTime"),
                    style: CTheme.textLight11Black(),
                  ),
                  AutoSizeText(
                    Localization.stLocalized("recieptNumber"),
                    style: CTheme.textLight11Black(),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
            )
          ],
        ),
        decoration: AppUtils.circularBoxDecoration(
            background: CColor.appWhite, border: CColor.appGreyLight),
      ),
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ReceiptDetailsClueOne()));
      },
    );
  }

  _clueTwoContainer() {
    return GestureDetector(
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, left: 15),
                  child: Text(
                    Localization.stLocalized("clueTwoPurchased"),
                    style: CTheme.textRegular14Black(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 15),
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.redAccent,
                          width: 1,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.redAccent,
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  padding:
                      EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  child: Image.asset(
                      "assets/images/triangle/triangle_clue_two.png"),
                  decoration: AppUtils.circularBoxDecoration(
                      background: CColor.appWhite, border: CColor.appGreyLight),

                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0, top: 10),
                    child: AutoSizeText(
                      Localization.stLocalized("codeMessage"),
                      style: CTheme.textLight14Black(),
                    ),
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.start,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  AutoSizeText(
                    Localization.stLocalized("dateTime"),
                    style: CTheme.textLight11Black(),
                  ),
                  AutoSizeText(
                    Localization.stLocalized("recieptNumber"),
                    style: CTheme.textLight11Black(),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
            )
          ],
        ),
        decoration: AppUtils.circularBoxDecoration(
            background: CColor.appWhite, border: CColor.appGreyLight),
      ),
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ReceiptDetailsClueTwo()));
      },
    );
  }
}
