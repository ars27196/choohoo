import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class ReceiptDetailsUnits extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReceiptDetailsUnitsState();
  }
}

class ReceiptDetailsUnitsState extends State<ReceiptDetailsUnits> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBarBuy(),
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            child: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Column(children: <Widget>[
                SizedBox(height: 20),
                AutoSizeText(
                  Localization.stLocalized("receiptDetails").toUpperCase(),
                  style: CTheme.textRegular16Black(),
                  textAlign: Localization.textAlignLeft(),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      Localization.stLocalized("unitsPurchased"),
                      style: CTheme.textRegular16Black(),
                    ),
                    Text(
                      Localization.stLocalized("recieptNumber"),
                      style: CTheme.textRegular12Black(),
                    ),
                  ],
                ),
                Row(children: <Widget>[
                  Text(
                    Localization.stLocalized("dateTimePurchased"),
                    style: CTheme.textLight14Black(),
                  ),
                ]),
                SizedBox(height: 10),
                Row(children: <Widget>[
                  Text(
                    Localization.stLocalized("gameUnitNumber"),
                    style: CTheme.textLight14Black(),
                  ),
                  Expanded(
                    child: Text(
                      Localization.stLocalized("unitDummy"),
                      style: CTheme.textLight14Black(),
                    ),
                  ),
                ]),
                Padding(
                  padding: const EdgeInsets.only(top:12.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        Localization.stLocalized("receiptPersonalDetail"),
                        style: CTheme.textLight14Black(),
                      ),
                    ],
                  ),
                ),
                _unitDetailsTable(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(child: Center(child: Text(Localization.stLocalized("receiptMailed"))))
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:18.0, bottom: 20),
                  child: Center(
                    child: Image.asset("assets/images/triangle/triangle_units.png"),
                  ),
                )
              ]),
            ),
          ),
        ));
  }

  _unitDetailsTable() {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(157, 155, 157, 0.2),
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  alignment: Alignment.center,
                  child: Text(
                    'Item',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(157, 155, 157, 0.2),
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  child: Text(
                    'Quantity',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(157, 155, 157, 0.2),
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  child: Text(
                    'Price',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  alignment: Alignment.center,
                  child: Text(
                    'Riddle',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  child: Text(
                    '1',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  child: Text(
                    'R50.00',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          top: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          bottom: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          left: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          right: BorderSide.none)),
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(
                      '',
                      style: TextStyle(
                        color: Color.fromRGBO(70, 69, 69, 1),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          top: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          bottom: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          right: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          left: BorderSide.none)),
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(
                      'VAT',
                      style: TextStyle(
                        color: Color.fromRGBO(70, 69, 69, 1),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  child: Text(
                    'R?????',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          top: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          bottom: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          left: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          right: BorderSide.none)),
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(
                      '',
                      style: TextStyle(
                        color: Color.fromRGBO(70, 69, 69, 1),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          top: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          bottom: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          right: BorderSide(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            style: BorderStyle.solid,
                          ),
                          left: BorderSide.none)),
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(
                      'Total',
                      style: TextStyle(
                        color: Color.fromRGBO(70, 69, 69, 1),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        style: BorderStyle.solid,
                      )),
                  child: Text(
                    'R50.00',
                    style: TextStyle(
                      color: Color.fromRGBO(70, 69, 69, 1),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
