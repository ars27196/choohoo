import 'dart:async';

import 'package:flutter/material.dart';
import 'package:world/routes/locationPermission/location_permission_page.dart';



class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}


class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 3),

            () => changeScreen());

//                Navigator.of(context).pushReplacement(MaterialPageRoute(
//            builder: (BuildContext context) => LocationPermissionPage())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Image.asset('assets/splash/splash_icon.png'),
      ),
    );
  }

  void changeScreen(){

    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => LocationPermissionPage()));

  }
}