import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:intl/intl.dart';
import 'package:world/routes/signup/signup_profile_pic_page.dart';
import 'package:world/utils/AppBarSignup.dart';
import 'package:world/utils/CustomRadio.dart';
import 'package:world/utils/city_country_picker/country_picker.dart' as countryPicker;

class SignupNamePage extends StatefulWidget {
  @override
  _SignupNamePage createState() => _SignupNamePage();
}

class _SignupNamePage extends State<SignupNamePage> {
  bool allowVal = false;
  bool blockVal = true;
  bool wedVal = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey();
  String _valueSQ;
  String _valueNC;
  String _valueDOB = "Date Of Birth dd/mm/yy*";
  DateTime _valueinDTO;
  bool _obscureText1 = true;
  bool _obscureText2 = true;
  Country _selected;

  TextEditingController countryTextController = TextEditingController();

  List<DropdownMenuItem<String>> listOfDropdown = [];

  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: AppBarSignup(),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Container(
                    height: 80,
                    width: 180,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/splash/chohoo_img.png'),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Text("      if yoo find it,\n   it's for yoo hoo   ",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w300)),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Text("SIGN UP",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Color.fromRGBO(180, 179, 180, 1),
                          fontSize: 18)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Name*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Surname*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Nickname or Alias (Optional)',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Email*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Email again to confirm*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              obscureText: _obscureText1,
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  width: 70,
                                  child: FlatButton(
                                      splashColor: Colors.transparent,
                                      onPressed: _showPasswordToggle1,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: new Text(
                                            _obscureText1 ? "Show" : "Hide",
                                            style:
                                                TextStyle(color: Color.fromRGBO(180, 179, 180, 1)),
                                          ))),
                                ),
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Create Password*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              obscureText: _obscureText2,
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  width: 70,
                                  child: FlatButton(
                                      splashColor: Colors.transparent,
                                      onPressed: _showPasswordToggle2,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: new Text(
                                            _obscureText2 ? "Show" : "Hide",
                                            style:
                                                TextStyle(color: Color.fromRGBO(180, 179, 180, 1)),
                                          ))),
                                ),
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Enter Password again to confirm*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 15, 16, 0),
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: Colors.white)),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Theme(
                        data:
                            Theme.of(context).copyWith(canvasColor: Color.fromRGBO(70, 69, 69, 1)),
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            underline: Container(),
                            iconEnabledColor: Colors.white,
                            iconDisabledColor: Colors.white,
                            hint: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Security Question*',
                                style: TextStyle(
                                    fontFamily: 'Roboto', fontSize: 16, color: Colors.white),
                              ),
                            ),
                            isExpanded: true,
                            items: dropDownMenuItems(),
                            onChanged: (value) {
                              setState(() {
                                _valueSQ = value;
                              });
                            },
                            value: _valueSQ,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Security Answer*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                  GestureDetector(
                    onTap: () async {
                      await countryPicker.showCountryPicker(context: context).then((value) {
                        setState(() {
                          _selected = value;
                        });
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Form(
  //                    key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                              child: Container(
                                alignment: Alignment.centerLeft,
                                height: 50,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(70, 69, 69, 1),
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(color: Colors.white)),
                                child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                    child: Container(
                                      child: Row(
                                        children: [
                                          _selected == null
                                              ? Container(
                                                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                                                  alignment: Alignment.centerLeft,
                                                  child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                                      child: Text(
                                                        "Country*",
                                                        style: TextStyle(
                                                            fontFamily: 'Roboto',
                                                            fontSize: 16,
                                                            color: Colors.white),
                                                      )),
                                                )
                                              : Container(
                                                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                                                  alignment: Alignment.centerLeft,
                                                  child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                                      child: Text(
                                                        "${_selected.name}",
                                                        style: TextStyle(
                                                            fontFamily: 'Roboto',
                                                            fontSize: 16,
                                                            color: Colors.white),
                                                      )),
                                                )
                                        ],
                                      ),
                                    )),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'City*',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 15, 16, 0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(0),
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(0)),
                            border: Border.all(color: Colors.white)),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                          child: Theme(
                            data: Theme.of(context)
                                .copyWith(canvasColor: Color.fromRGBO(70, 69, 69, 1)),
                            child: Text(
                              _selected == null ? "+27" : "+" + _selected.dialingCode,
                              style: TextStyle(
                                  fontFamily: 'Roboto', fontSize: 16, color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(0),
                                bottomRight: Radius.circular(20),
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(20)),
                            border:
                                Border.all(color: Colors.white, width: 1, style: BorderStyle.solid),
                            color: Color.fromRGBO(70, 69, 69, 1),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              keyboardType: TextInputType.number,
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                hintText: 'Mobile Number*',
                                hintStyle: TextStyle(color: Colors.white),
                                border: InputBorder.none,
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Color.fromRGBO(70, 69, 69, 1),
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 15, 16, 0),
                  child: GestureDetector(
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(70, 69, 69, 1),
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: Colors.white)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 5, 40, 5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                height: 50,
                                child: Text(
                                  _valueDOB,
                                  style: TextStyle(color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      dateTimePicker();
                    },
                  ),
                ),
                Container(
                  child: CustomRadio(),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Facebook Handle @Name',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Twitter Handle @Name',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'Instagram Handle @Name',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Form(
//                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            height: 50,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                                hintText: 'TikTok Handle @Name',
                                hintStyle: TextStyle(color: Colors.white),
                                filled: true,
                                fillColor: Color.fromRGBO(70, 69, 69, 1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(const Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 25, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: Colors.white)),
                        child: Theme(
                          data: ThemeData(
                            unselectedWidgetColor: Color.fromRGBO(70, 69, 69, 1),
                          ),
                          child: Checkbox(
                              activeColor: Color.fromRGBO(70, 69, 69, 1),
                              checkColor: Colors.white,
                              value: allowVal,
                              onChanged: (bool value) {
                                setState(() {
                                  allowVal = value;
                                });
                                if (value == true) {
                                  setState(() {
                                    blockVal = false;
                                  });
//                                Navigator.pushReplacement(context, MaterialPageRoute(
//                                    builder: (context) => MyHomePage()),
//                                Navigator.push(
//                                  context,
//                                  MaterialPageRoute(
//                                      builder: (context) => MyHomePage()),
//                                );
                                } else {
                                  setState(() {
                                    blockVal = true;
                                  });
                                }
                              }),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Container(
                            child: Text(
                              'By Signing Up you agree to our Terms,' +
                                  '\nPrivacy Policy and End User License Agreement',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 40, 0, 60),
                  child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: Colors.white)),
                    child: IconButton(
                      icon: Icon(Icons.arrow_forward),
                      iconSize: 22,
                      color: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => SignupProfilePicPage()),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> dropDownMenuItems() {
    return [
      DropdownMenuItem(
        value: "1",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "a.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "What Is your favourite book?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "2",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 20,
              child: Text(
                "b.	",
                style: TextStyle(color: Colors.white),
              ),
            ),
            Expanded(
              child: Text(
                "What is the name of the road you grew "
                "up on?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "3",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "c.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "What is your mother’s maiden name?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "4",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "d.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "What is the name of your favourite "
                "pet?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "5",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "e.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "Where did you meet your spouse?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "6",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "f.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "What was the first company that you "
                "worked for?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "7",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "g.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "Where did you go to high school?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "8",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "h.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "What is your favourite food?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "9",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "i.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "What city were you born in?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "10",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "j.	",
              style: TextStyle(color: Colors.white),
            ),
            Expanded(
              child: Text(
                "Where is your favourite place for "
                "a vacation?",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  Future dateTimePicker() {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
              color: Color.fromRGBO(70, 69, 69, 1),
              height: MediaQuery.of(context).copyWith().size.height / 2.5,
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      child: Container(
                        alignment: Alignment.center,
                        height: 42,
                        color: Color.fromRGBO(70, 69, 69, 1),
                        child: Text(
                          'SELECT',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          String formattedDate = DateFormat('dd - MMM - yyyy').format(_valueinDTO);
                          _valueDOB = formattedDate;
                        });
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Container(
                      child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        backgroundColor: Colors.white,
                        onDateTimeChanged: (DateTime dateTime) {
                          _valueinDTO = dateTime;
                        },
                        mode: CupertinoDatePickerMode.date,
                      ),
                    ),
                  ),
                ],
              ));
        });
  }

  void _showPasswordToggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _showPasswordToggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }
}
