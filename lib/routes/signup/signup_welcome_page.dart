
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/main/main_page.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/AppBarSignup.dart';

class SignupWelcomePage extends StatefulWidget {
  final bool fromEditProfile;

  SignupWelcomePage({this.fromEditProfile});

  @override
  _SignupWelcomePage createState() => _SignupWelcomePage(fromEditProfile);
}

class _SignupWelcomePage extends State<SignupWelcomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  bool fromEditProfile;

  _SignupWelcomePage(this.fromEditProfile);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: fromEditProfile ? AppBarBuy() : AppBarSignup(),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                fromEditProfile
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                        child: Container(
                          height: 80,
                          width: 180,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ),
                fromEditProfile
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: Text(
                            "      if yoo find it,\n   it's for yoo hoo   ",
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                color: Colors.white,
                                fontSize: 25,
                                fontWeight: FontWeight.w300)),
                      ),
                fromEditProfile
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                        child: Text("SIGN UP",
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                color: Color.fromRGBO(180, 179, 180, 1),
                                fontSize: 18)),
                      ),
                _messageContainer(context),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                  child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(70, 69, 69, 1),
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: Colors.white)),
                    child: IconButton(
                      icon: Icon(Icons.arrow_forward),
                      iconSize: 22,
                      color: Colors.white,
                      onPressed: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MainPage()),
                        )
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _messageContainer(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double widgetWidth = screenWidth - 80;
    print(screenWidth);
    print(widgetWidth);
    return Center(
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
                    child: AutoSizeText(
                      fromEditProfile
                          ? Localization.stLocalized(
                              "welcomeMessageEditProfile")
                          : Localization.stLocalized("welcomeMessage"),
                      style: CTheme.textLight16Yellow(),
                    ),
                  ),
                ),
              ],
            ),


            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child:Padding(
                          padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                          child: Text(Localization.stLocalized("choohooIdNumber"),
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  color: Color.fromRGBO(180, 179, 180, 1),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w100)),
                        ),
                )
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                    child: AutoSizeText(
                        fromEditProfile
                            ? Localization.stLocalized(
                                "emailSentVerifyEditProfile")
                            : Localization.stLocalized("emailSentVerify"),
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Color.fromRGBO(180, 179, 180, 1),
                            fontSize: 16,
                            fontWeight: FontWeight.w100)),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                    child: AutoSizeText(
                        fromEditProfile
                            ? Localization.stLocalized("checkLinkEditProfile")
                            : Localization.stLocalized("checkLink"),
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Color.fromRGBO(180, 179, 180, 1),
                            fontSize: 16,
                            fontWeight: FontWeight.w100)),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(30, 10, 10, 0),
                      child: Text(
                        fromEditProfile
                            ? Localization.stLocalized("profileUpdatedMessage")
                            : Localization.stLocalized("detailsVerified"),
                        style: CTheme.textLight16Yellow(),
                      )),
                )
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: AutoSizeText(Localization.stLocalized("readyToHunt"),
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Color.fromRGBO(180, 179, 180, 1),
                            fontSize: 16,
                            fontWeight: FontWeight.w100)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
