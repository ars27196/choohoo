
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:world/constants/CTheme.dart';

import 'login_page.dart';


class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPage createState() => _ForgotPasswordPage();
}

class _ForgotPasswordPage extends State<ForgotPasswordPage> {
  bool allowVal = false;
  bool blockVal = true;
  bool wedVal = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar:AppBar(
        backgroundColor: Color.fromRGBO(112, 112, 112, 1),
        titleSpacing: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              iconSize: 40,
              color: Colors.white,
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            ),
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  onPressed: () => debugPrint('Back icon clicked'),
                ),
//                Positioned(
//                  top: 12.0,
//                  right: 10.0,
//                  width: 10.0,
//                  height: 10.0,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: Colors.black,
//                    ),
//                  ),
//                )
              ],
            ),
          ],
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        actions: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                color: Color.fromRGBO(112, 112, 112, 1),
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                onPressed: () {
                  Navigator.pop(context,
                    MaterialPageRoute(

                        builder: (context) => LoginPage()),);
                  debugPrint('Login Clicked');
                },
                child: Text(
                  "Login".toUpperCase(),
                  style: GoogleFonts.roboto(
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Container(
                    height: 80,
                    width: 180,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/splash/chohoo_img.png'),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Text("      if yoo find it,\n   it's for yoo hoo   ",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.white,
                          fontSize: 25,
                      fontWeight: FontWeight.w300)),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Text("FORGOT PASSWORD & LOGIN DETAILS",
                      style: CTheme.textRegular16LightGrey()),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 15, 15, 0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 50,
                          child: TextFormField(
                            style: TextStyle(
                              color: Colors.white
                            ),
                            cursorColor: Colors.white,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                              hintText: 'Enter email to reset your password',
                              hintStyle: CTheme.textRegular16White(),
                              filled: true,
                              fillColor: Color.fromRGBO(70, 69, 69, 1),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 1,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.all(
                                      const Radius.circular(20))),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 20, 16, 0),
                          child: Text("The email address you have entered does not "
                              "match any of the email addresses in our"
                              " system.",
                              style: CTheme.textLight16Yellow()),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 10, 16, 0),
                          child: Text("Please check the spelling of your email address or try an alternate email address.",
                              style: CTheme.textLight16White()),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 10, 16, 0),
                          child: Text("If you  do not  know which  email address  you "
                              "signed up with, please try entering all your "
                              "email address till you get a match with our system.",
                              style: CTheme.textLight16White()),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 10,16, 0),
                          child: Text("An email  has  been sent  to you  to reset"
                              " your password.",
                              style: CTheme.textLight16Yellow()),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 10, 16, 0),
                          child: Text("Kindly click the link in your email to"
                              "reset your password.",
                              style: CTheme.textLight16White()),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 10, 16, 40),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text("Your password has been reset,"
                                    " please Login.",
                                    style: CTheme.textLight16Yellow()),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
