
import 'package:world/routes/signup/signup_name_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:world/utils/HandleNavigation.dart';
import 'forget_password_page.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  bool allowVal = false;
  bool blockVal = true;
  bool wedVal = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(112, 112, 112, 1),
        titleSpacing: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  onPressed: ()
                  {
                    if(HandleNavigation.appExitCheck == true)
                      {
                        SystemNavigator.pop();
                      }
                    Navigator.pop(context);

                  },
                ),
//                Positioned(
//                  top: 12.0,
//                  right: 10.0,
//                  width: 10.0,
//                  height: 10.0,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: Colors.black,
//                    ),
//                  ),
//                )
              ],
            ),
          ],
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        actions: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                color: Color.fromRGBO(112, 112, 112, 1),
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                onPressed: () {
                  Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => SignupNamePage()),);
                  debugPrint('Login Clicked');
                },
                child: Text(
                  "SIGN UP",
                  style: GoogleFonts.roboto(
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(
            child: Column(
                children: <Widget>[
            Padding(
            padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
            child: Container(
              height: 80,
              width: 180,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/splash/chohoo_img.png'),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Text("      if yoo find it,\n   it's for yoo hoo   ",
                style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.w300)),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
            child: Text("LOGIN",
                style: TextStyle(
                  fontFamily: 'Roboto',
                  color: Color.fromRGBO(180, 179, 180, 1),
                  fontSize: 18,
                )),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Container(
                      height: 50,
                      child: TextFormField(
                        style: TextStyle(
                            color: Colors.white
                        ),
                        cursorColor: Colors.white,
                        decoration: InputDecoration(
                          contentPadding:
                          EdgeInsets.fromLTRB(18, 14, 18, 14),
                          hintText: 'Email',
                          hintStyle: TextStyle(color: Colors.white),
                          filled: true,
                          fillColor: Color.fromRGBO(70, 69, 69, 1),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 1,
                              style: BorderStyle.solid,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(
                                  const Radius.circular(20))),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
            child: Form(
//                    key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Container(
                      height: 50,
                      child:
                      TextFormField(
                        style: TextStyle(
                            color: Colors.white
                        ),
                        cursorColor: Colors.white,
                        decoration: InputDecoration(
                          contentPadding:
                          EdgeInsets.fromLTRB(18, 14, 18, 14),
                          hintText: 'Password*',
                          hintStyle: TextStyle(color: Colors.white),
                          filled: true,
                          fillColor: Color.fromRGBO(70, 69, 69, 1),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 1,
                              style: BorderStyle.solid,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(
                                  const Radius.circular(20))),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 30, 0),
            child: Container(
              alignment: Alignment.centerRight,
              child: FlatButton(
                color: Colors.black,
                textColor: Colors.white,
                padding: EdgeInsets.all(2.0),
                onPressed: () {
                  Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => ForgotPasswordPage()),);
                },
                child: Text(
                  "Forgot password?",
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      color: Color.fromRGBO(180, 179, 180, 1)
                    ),
                    fontSize: 16,

                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(70, 69, 69, 1),
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(color: Colors.white)),
              child: IconButton(
                  icon: Icon(Icons.arrow_forward),
                  iconSize: 22,
                  color: Colors.white,
                  onPressed: () => {

                  Navigator.of(context).pushNamedAndRemoveUntil(
                  'MAIN_PAGE', (Route<dynamic> route) => false),


//                        Navigator.push(c ontext,
//                          MaterialPageRoute(
//                              builder: (context) => MainPage()),
//                        )
              },
            ),
          ),
        ),
        ],
      ),
    ),)
    ,
    )
    ,
    );
  }
}
