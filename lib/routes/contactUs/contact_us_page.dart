import 'package:auto_size_text/auto_size_text.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/city_country_picker/country_picker.dart' as countryPicker;

class ContactUsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ContactUsState();
  }
}

class ContactUsState extends State<ContactUsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  Country _selected;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBarBuy(),
      body: SingleChildScrollView(
        child: Container(
            width: double.infinity,
            child: Container(
              child: Column(children: <Widget>[
                SizedBox(height: 20),
                AutoSizeText(
                  Localization.stLocalized("contactUs").toUpperCase(),
                  style: CTheme.textRegular16DarkGrey(),
                  textAlign: Localization.textAlignLeft(),
                ),
                SizedBox(height: 20),
                Container(
                    padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                            text: Localization.stLocalized("contactUsNote"),
                            style: CTheme.textLight16Black(),
                            children: <TextSpan>[
                              TextSpan(
                                  text: Localization.stLocalized("emailAddressContact"),
                                  style: TextStyle(
                                      fontFamily: CTheme.defaultFont(),
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.w200))
                            ]),
                      ),
                    )),
                _photoOrVideoContainer()
              ]),
            )),
      ),
    );
  }

  String _valueEnquiry;

  Widget _formContainer() {
    return Column(
      children: <Widget>[
        Form(
          //key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 10, 16, 0),
                child: Container(
                  height: 50,
                  child: TextFormField(
                    style: TextStyle(color: Colors.white),
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                      hintText: 'Name*',
                      hintStyle: TextStyle(color: Colors.white),
                      filled: true,
                      fillColor: Color.fromRGBO(70, 69, 69, 1),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(const Radius.circular(20))),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Form(
//                 key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 10, 16, 0),
                child: Container(
                  height: 50,
                  child: TextFormField(
                    style: TextStyle(color: Colors.white),
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                      hintText: 'Surname*',
                      hintStyle: TextStyle(color: Colors.white),
                      filled: true,
                      fillColor: Color.fromRGBO(70, 69, 69, 1),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(const Radius.circular(20))),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () async {
            await countryPicker.showCountryPicker(context: context).then((value) {
              setState(() {
                _selected = value;
              });
            });
          },
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Form(
              //                    key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Container(
                      alignment: Alignment.centerLeft,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(70, 69, 69, 1),
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: Colors.white)),
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Container(
                            child: Row(
                              children: [
                                _selected == null
                                    ? Container(
                                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                            child: Text(
                                              "Country*",
                                              style: TextStyle(
                                                  fontFamily: 'Roboto',
                                                  fontSize: 16,
                                                  color: Colors.white),
                                            )),
                                      )
                                    : Container(
                                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                            child: Text(
                                              "${_selected.name}",
                                              style: TextStyle(
                                                  fontFamily: 'Roboto',
                                                  fontSize: 16,
                                                  color: Colors.white),
                                            )),
                                      )
                              ],
                            ),
                          )),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Form(
//                    key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      cursorColor: Colors.white,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                        hintText: 'City*',
                        hintStyle: TextStyle(color: Colors.white),
                        filled: true,
                        fillColor: Color.fromRGBO(70, 69, 69, 1),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: Colors.white,
                            width: 1,
                            style: BorderStyle.solid,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(const Radius.circular(20))),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Form(
//                    key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      cursorColor: Colors.white,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(18, 14, 18, 14),
                        hintText: 'Email*',
                        hintStyle: TextStyle(color: Colors.white),
                        filled: true,
                        fillColor: Color.fromRGBO(70, 69, 69, 1),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: Colors.white,
                            width: 1,
                            style: BorderStyle.solid,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(const Radius.circular(20))),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 15, 16, 0),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
                color: Color.fromRGBO(70, 69, 69, 1),
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Colors.white)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
              child: Theme(
                data: Theme.of(context).copyWith(canvasColor: Color.fromRGBO(70, 69, 69, 1)),
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButton<String>(
                    underline: Container(),
                    iconEnabledColor: Colors.white,
                    iconDisabledColor: Colors.white,
                    hint: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Enquiry',
                        style: TextStyle(fontFamily: 'Roboto', fontSize: 16, color: Colors.white),
                      ),
                    ),
                    isExpanded: true,
                    items: [
                      DropdownMenuItem(
                        value: "1",
                        child: Row(
                          children: <Widget>[
                            Text(
                              "a.	",
                              style: TextStyle(color: Colors.white),
                            ),
                            Expanded(
                              child: Text(
                                "Enquiry 1",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                    onChanged: (value) {
                      setState(() {
                        _valueEnquiry = value;
                      });
                    },
                    value: _valueEnquiry,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _photoOrVideoContainer() {
    return Container(
      height: 160,
      padding: EdgeInsets.only(top: 30, bottom: 30, left: 5, right: 5),
      margin: EdgeInsets.only(left: 16, right: 16, top: 15),
      child: Row(
        children: [
          Expanded(
            child: Text(
              Localization.stLocalized("photoVideo").toUpperCase(),
              textAlign: TextAlign.center,
              style: CTheme.textRegular16LightGrey(),
            ),
          )
        ],
      ),
      decoration: BoxDecoration(
        color: CColor.appWhite,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        border: Border(
            bottom: BorderSide(color: CColor.appGreyLight),
            top: BorderSide(color: CColor.appGreyLight),
            right: BorderSide(color: CColor.appGreyLight),
            left: BorderSide(color: CColor.appGreyLight)),
      ),
    );
  }
}
