import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/notification/notification_detail_page.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/app_utils.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationPageState();
  }
}

class NotificationPageState extends State<NotificationPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBarBuy(),
      body: SingleChildScrollView(
          child: Container(
        width: double.infinity,
        child: Column(children: <Widget>[
          SizedBox(height: 20),
          AutoSizeText(
            Localization.stLocalized("notifications").toUpperCase(),
            style: CTheme.textRegular16Black(),
            textAlign: Localization.textAlignLeft(),
          ),
          _searchBar(),
          ListView(
            shrinkWrap: true,
            children: _listOfNotifications(),
          )
        ]),
      )),
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 20, 16, 0),
      child: Container(
        alignment: Alignment.center,
        height: 50,
        child: TextFormField(
          style: TextStyle(color: Colors.white),
          cursorColor: Colors.white,
          decoration: InputDecoration(
            suffixIcon: IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: () => {}),
            contentPadding: EdgeInsets.fromLTRB(18, 0, 18, 0),
            hintText: 'Search',
            hintStyle: TextStyle(color: Colors.white, fontSize: 16),
            filled: true,
            fillColor: Color.fromRGBO(70, 69, 69, 1),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.white,
                width: 1,
                style: BorderStyle.solid,
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                  width: 1,
                ),
                borderRadius: BorderRadius.all(const Radius.circular(20))),
          ),
        ),
      ),
    );
  }

  _listOfNotifications() {
    return [
      Container(
        child: GestureDetector(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, left: 15),
                    child: Text(
                      Localization.stLocalized("notificationTitle"),
                      style: CTheme.textRegular14Black(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, right: 16),
                    child: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.redAccent,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.redAccent,
                      ),
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
              Row(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(40),
                      decoration: AppUtils.circularBoxDecoration(
                          background: CColor.appWhite,
                          border: CColor.appGreyLight),
                     ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0, top: 10, bottom: 5),
                      child: AutoSizeText(
                        Localization.stLocalized("notificationDetailText"),
                        style: CTheme.textLight14Black(),
                      ),
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.start,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      Localization.stLocalized("dateTime"),
                      style: CTheme.textLight11Black(),
                    ),
                    AutoSizeText(
                      Localization.stLocalized("notificationNumber"),
                      style: CTheme.textLight11Black(),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
              )
            ],
          ),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => NotificationDetailPage()));
          },
        ),
        margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 15),
        decoration: AppUtils.circularBoxDecoration(
            background: CColor.appWhite, border: CColor.appGreyLight),
      )
    ];
  }
}
