import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/app_utils.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotificationDetailPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationDetailState();
  }
}

class NotificationDetailState extends State<NotificationDetailPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBarBuy(),
        body: SingleChildScrollView(
            child: Container(
                width: double.infinity,
                child: Column(children: <Widget>[
                  SizedBox(height: 20),
                  AutoSizeText(
                    Localization.stLocalized("notificationDetail")
                        .toUpperCase(),
                    style: CTheme.textRegular16Black(),
                    textAlign: Localization.textAlignLeft(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 15),
                        child: Text(
                          Localization.stLocalized("notificationNumber"),
                          style: CTheme.textRegular16Black(),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20, top: 15),
                        child: Text(
                          Localization.stLocalized("notificationTitle"),
                          style: CTheme.textRegular12Black(),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 5),
                        child: Text(Localization.stLocalized("dateTime"),
                            style: CTheme.textLight11Black()),
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width - 40,
                    height: 160,
                    decoration: AppUtils.circularBoxDecoration(
                        background: CColor.appWhite,
                        border: CColor.appGreyLight),
                    child: Center(
                      child: AutoSizeText(
                        Localization.stLocalized("mediaGoesHere"),
                        textAlign: TextAlign.center,
                        style: CTheme.textRegular14Black(),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(16),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            Localization.stLocalized("notificationDummyText"),
                            style: CTheme.textLight16Black(),
                          ),
                        )
                      ],
                    ),
                  )
                ]))));
  }
}
