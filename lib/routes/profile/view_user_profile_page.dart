import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/appPromoCode/app_promo_code.dart';
import 'package:world/routes/buyGameUnits/buy_game_units_page.dart';
import 'package:world/routes/codes/code_page.dart';
import 'package:world/routes/points/my_points_page.dart';
import 'package:world/routes/profile/edit_user_profile_page.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/enums.dart';

class ViewUserProfilePage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: AppBarBuy(),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              AutoSizeText(
                Localization.stLocalized("profileWallet").toUpperCase(),
                style: CTheme.textRegular16White(),
                textAlign: Localization.textAlignLeft(),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: _profileImage(),
              ),
              SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: _profileDetails(context),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                child: Container(
                  width: 110,
                  height: 50,
                  child: FlatButton(
                    child: AutoSizeText(
                      Localization.stLocalized("editInfo"),
                      style: CTheme.textRegular16White(),
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditUserProfilePage()),
                      );
                    },
                  ),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(70, 69, 69, 1),
                      borderRadius: BorderRadius.circular(18),
                      border: Border.all(color: Colors.white)),
                  padding: EdgeInsets.all(15),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _profileImage() {
    return Container(
        width: 135,
        height: 135,
        decoration: BoxDecoration(
            color: Color.fromRGBO(70, 69, 69, 1),
            borderRadius: BorderRadius.circular(100),
            border: Border.all(color: Colors.white)),
        child: Center(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: IconButton(
                    icon: Icon(Icons.person),
                    iconSize: 80,
                    color: Colors.white,
                    onPressed: () => {},
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(3, 0, 0, 0),
                  child: IconButton(
                    icon: Icon(Icons.add),
                    iconSize: 30,
                    color: Colors.white,
                    onPressed: () => {},
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  _profileDetails(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
          child: _profileDataRow(title: "choohooId", value: "ChoohooId2332"),
        ),
        _profileDataRow(
            title: "availableGameUnit",
            value: "20000000",
            function: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => BuyGameUnitsPage()),
              );
            }),
        _profileDataRow(
            title: "availablePoints",
            value: "106666666666666666660",
            function: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyPointsPage()),
              );
            }),
        _profileDataRow(
            title: "availableSpecialCode",
            value: "287897977979978797797979",
            function: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CodePage(
                          codeType: CodeType.specialCode,
                        )),
              );
            }),
        _profileDataRow(
            title: "availableSuperSpecialCode",
            value: "1686876868686876767868687",
            function: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CodePage(
                          codeType: CodeType.superSpecialCode,
                        )),
              );
            }),
        _profileDataRow(
            title: "promoCodePoints",
            value: "60098080808009809808080808",
            function: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AppPromoCodePage()),
              );
            }),
        SizedBox(height: 30),
        _profileDataRow(title: "name", value: "Karlind"),
        _profileDataRow(title: "surname", value: "Govender"),
        _profileDataRow(title: "nickname", value: "Karllllllllllllllllllllllllllll8687685765545646546rtcgfxfgvhvjbnjknkjbhj"),
        _profileDataRow(title: "email", value: "karl123@gmail.commmmmmmmmmmmmmmmmmmm"),
        _profileDataRow(title: "mobileNumber", value: "+32 333 444442324"),
        _profileDataRow(title: "country", value: "South Africa"),
        _profileDataRow(title: "city", value: "Cape Town"),
        _profileDataRow(title: "dateOfBirth", value: "27/09/1990"),
        _profileDataRow(title: "gender", value: "Male"),
      ],
    );
  }

  _profileDataRow({String title, String value, Function function}) {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: function,
          child: AutoSizeText(
            Localization.stLocalized(title)+" ",
            style: CTheme.textRegular16White(),
            textAlign: Localization.textAlignLeft(),
          ),
        ),

        Expanded(
          child: AutoSizeText(
            value,
            style: CTheme.textRegular16White(),
            textAlign: Localization.textAlignLeft(),
          ),
        ),
      ],
    );
  }
}
