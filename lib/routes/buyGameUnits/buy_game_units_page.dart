import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/utils/AppBarBuy.dart';

import 'buy_game_unit_payment_page.dart';
import 'load_unit_screen.dart';

class BuyGameUnitsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BuyGameUnitsState();
  }
}

class BuyGameUnitsState extends State<BuyGameUnitsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBarBuy(),
      body: SingleChildScrollView(
          child: Container(
              width: double.infinity,
              child: Container(
                child: Column(children: <Widget>[
                  SizedBox(height: 20),
                  AutoSizeText(
                    Localization.stLocalized(
                      "buyLoadGameUnits",
                    ).toUpperCase(),
                    style: CTheme.textRegular16Black(),
                    textAlign: Localization.textAlignLeft(),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 20),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              Localization.stLocalized("purcahasedDate"),
                              style: CTheme.textRegular12Black(),
                            ),
                            Text(" 200"),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              Localization.stLocalized("usedData"),
                              style: CTheme.textRegular12Black(),
                            ),
                            Text(" 200"),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              Localization.stLocalized("Expired"),
                              style: CTheme.textRegular12Black(),
                            ),
                            Text(" 150"),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              Localization.stLocalized("Available"),
                              style: CTheme.textRegular12Black(),
                            ),
                            Text(" 50"),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 20),
                    child: Column(
                      children: <Widget>[
                        Row(children: <Widget>[
                          Expanded(
                              child: Text(
                            Localization.stLocalized("valid12Months"),
                            style: CTheme.textLight16Black(),
                          )),
                        ])
                      ],
                    ),
                  ),
                  GestureDetector(
                    child: _loadGameUnit(),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoadUnitMessagePage()),
                      );
                    },
                  ),
                  SizedBox(height: 10),
                  Stack(
                    children: <Widget>[
                      Divider(
                        thickness: 1.0,
                        color: CColor.appGreyDark,
                      ),
                      Positioned.fill(
                          child: Align(
                              alignment: Alignment.center,
                              child: Container(
                                  color: CColor.appWhite,
                                  child: Text(
                                    "  Or  ",
                                    style: CTheme.textLight16Black(),
                                  ))))
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                    child: Text(
                      Localization.stLocalized("buyNow").toUpperCase(),
                      style: CTheme.textRegular16Black(),
                    ),
                  ),
                  _clueContainer(),
                  SizedBox(height: 10),
                  Center(
                    child: Column(
                      children: <Widget>[
                        Text(Localization.stLocalized("quantity"),
                            style: CTheme.textBold12LightGrey()),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IconButton(
                                  icon: Icon(Icons.remove),
                                  onPressed: () {
                                    setState(() {
                                      if (counter > 0) counter--;
                                    });
                                  },
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 16, bottom: 16, left: 30, right: 30),
                                child: Text("$counter"),
                                decoration: BoxDecoration(
                                    border: Border.all(),
                                    borderRadius: BorderRadius.all(Radius.circular(20))),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IconButton(
                                  icon: Icon(Icons.add),
                                  onPressed: () {
                                    setState(() {
                                      counter++;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(70, 69, 69, 1),
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.white)),
                      child: IconButton(
                        icon: Icon(Icons.arrow_forward),
                        iconSize: 22,
                        color: Colors.white,
                        onPressed: () => {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => BuyGameUnitPaymentPage()),
                          )
                        },
                      ),
                    ),
                  ),
                ]),
              ))),
    );
  }

  Widget _loadGameUnit() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 15, 16, 5),
      child: Container(
        alignment: Alignment.center,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 8, bottom: 8, right: 12),
              child: Text(
                Localization.stLocalized("loadGameUnit").toUpperCase(),
                style: CTheme.textRegular14White(),
              ),
            )),
            Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: Image.asset("assets/images/forward_white/forward_white.png"),
            )
          ],
        ),
        decoration: BoxDecoration(
            color: CColor.appGreyDark,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
    );
  }

  Widget _clueContainer() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: Container(
        height: 160,
        decoration: BoxDecoration(
            border: Border.all(
              color: Color.fromRGBO(70, 69, 69, 1),
            ),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: IconButton(
                            icon: Image.asset(
                              'assets/images/triangle/triangle_units.png',

                            ),
                            color: Colors.black,
                            onPressed: () => {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                    child: Container(
                      child: Text(
                        Localization.stLocalized("r50"),
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                    ),
                  )
                ],
              ),
            ),
            /* Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 10, 5),
                    child: Text(
                      '',
                      style: TextStyle(
                          color: Color.fromRGBO(70, 69, 69, 1), fontWeight: FontWeight.w100),
                    ),
                  )
                ],
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}
