
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/buyGameUnits/payment_message_screen.dart';
import 'package:world/utils/AppBarBuy.dart';

class PaymentCardDetailPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PaymentCardDetailState();
  }
}

class PaymentCardDetailState extends State<PaymentCardDetailPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        bottomNavigationBar: _paymentSecuredBotomBar(),
        appBar: AppBarBuy(),
        body: SingleChildScrollView(
            child: Container(
                width: double.infinity,
                child: Container(
                  child: Column(
                      children: <Widget>[
                    SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.only(left: 20.0, right: 20,),
                      child: AutoSizeText(
                        Localization.stLocalized(
                          "gameUnitDebitCreditCard",
                        ).toUpperCase(),
                        style: CTheme.textRegular16DarkGrey(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20.0, right: 20, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                         Expanded(
                           child: Container(
                             child: Row(
                               children: [
                                 Expanded(
                                   child: Image.asset(
                                       "assets/images/cards_logo/mastercard.png"),
                                 ),
                                 Expanded(child: Image.asset("assets/images/cards_logo/maestro.png")),
                                 Expanded(child: Image.asset("assets/images/cards_logo/visa.png"))
                               ],
                             ),
                           ),
                         ),
                          Expanded(
                            child: Container(
                              child: Row(
                                children: <Widget>[
                                  Checkbox(
                                    focusColor: CColor.appBlackLight,
                                    tristate: false,
                                    value: true,
                                    onChanged: (a){

                                    },
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: Text(
                                        "Remember card for future use",
                                        style: CTheme.textRegular12LightGrey(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    _clueContainer(),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(157, 155, 157, 0.2),
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Item',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(157, 155, 157, 0.2),
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'Quantity',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(157, 155, 157, 0.2),
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'Price',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Riddle',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    '1',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'R50.00',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      '',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      'VAT',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'R?????',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      '',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide(
                                            color:
                                                Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      'Total',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'R50.00',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset("assets/images/locks/white_lock.png"),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                Localization.stLocalized("payNow"),
                                style: CTheme.textRegular16White(),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: CColor.appBlackLight,
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            border: Border.all()),
                      ),
                      onTap: (){
                        Navigator.push(context,
                          MaterialPageRoute(
                              builder: (context) => PaymentMessagePage()),
                        );

                      },
                    )

                  ]),
                ))));
  }

  Container _paymentSecuredBotomBar() {
    return Container(
      padding: EdgeInsets.only(right: 20),
      color: Color.fromRGBO(180, 179, 180, 1),
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: IconButton(
              icon: Icon(Icons.lock),
              iconSize: 20,
              color: Color.fromRGBO(106, 105, 106, 1),
              onPressed: () => {},
            ),
          ),
          Flexible(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                'Your payment information is secure.',
                textAlign: TextAlign.center,
                style: TextStyle(color: Color.fromRGBO(106, 105, 106, 1)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _clueContainer() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: Container(
        height: 160,
        decoration: BoxDecoration(
            border: Border.all(
              color: Color.fromRGBO(70, 69, 69, 1),
            ),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                  child: Text(
                    'June 19, 2019, Friday, 13:52',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontStyle: FontStyle.italic,
                      fontSize: 12,
                      color: Color.fromRGBO(180, 179, 180, 1),
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: IconButton(
                            icon: Image.asset(
                                'assets/images/triangle/triangle_units.png'),

                            color: Colors.black,
                            onPressed: () => {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                      child: Container(
                        child: Text(
                          Localization.stLocalized("r50"),
                          style: TextStyle(color: Color.fromRGBO(70, 69, 69, 1),
                              fontSize: 15),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(
                      'Game Units Number:',
                      style: TextStyle(
                          fontSize: 12,
                          color: CColor.appBlackLight,
                          fontWeight: FontWeight.w200),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(
                      'GUWAGenerated6digitNumber',
                      style: TextStyle(
                          fontSize: 12,
                          color: CColor.appBlackLight,
                          fontWeight: FontWeight.w200),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
