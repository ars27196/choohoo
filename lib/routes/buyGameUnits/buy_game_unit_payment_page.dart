
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/buyGameUnits/payment_cards_detail_page.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/CustomRadioBuy.dart';
import 'package:world/utils/CustomRadioBuyFalse.dart';

class BuyGameUnitPaymentPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BuyGameUnitPaymentState();
  }
}

class BuyGameUnitPaymentState extends State<BuyGameUnitPaymentPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  String imageValue= 'assets/images/mastercardimg.png';
  String _value;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        bottomNavigationBar: _paymentSecuredBotomBar(),
        appBar: AppBarBuy(),
        body: SingleChildScrollView(
            child: Container(
                width: double.infinity,
                child: Container(
                  child: Column(children: <Widget>[
                    SizedBox(height: 20),
                    AutoSizeText(
                      Localization.stLocalized(
                        "buyGameUnitsReview",
                      ).toUpperCase(),
                      style: CTheme.textRegular16DarkGrey(),
                      textAlign: Localization.textAlignLeft(),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          Localization.stLocalized("contactCustomerSupport"),
                          textAlign: TextAlign.center,
                          style: CTheme.textBold12LightGrey(),
                        ),
                      ),
                    ),
                    _clueContainer(),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(157, 155, 157, 0.2),
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Item',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(157, 155, 157, 0.2),
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'Quantity',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(157, 155, 157, 0.2),
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'Price',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Riddle',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    '1',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'R50.00',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      '',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      'VAT',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'R?????',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      '',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          top: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          bottom: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          right: BorderSide(
                                            color: Color.fromRGBO(70, 69, 69, 1),
                                            style: BorderStyle.solid,
                                          ),
                                          left: BorderSide.none)),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      'Total',
                                      style: TextStyle(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(70, 69, 69, 1),
                                        style: BorderStyle.solid,
                                      )),
                                  child: Text(
                                    'R50.00',
                                    style: TextStyle(
                                      color: Color.fromRGBO(70, 69, 69, 1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Text("PAYMENT METHOD",
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              color: Color.fromRGBO(70, 69, 69, 1),
                              fontSize: 18)),
                    ),


                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
                      child: Container(
                        height: 120,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Color.fromRGBO(70, 69, 69, 1),
                                style: BorderStyle.solid)),
                        child: Row(
                          children: <Widget>[
                            Container(
                                alignment: Alignment.topLeft,
                                height: 120,
                                width: 60,
                                child: CustomRadioBuy()),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                height: 120,
                                child: Container(
                                  height: 100,
                                  width: 120,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(

                                      fit: BoxFit.scaleDown,
                                      image: AssetImage(
                                          imageValue
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              color: Color.fromRGBO(70, 69, 69, 1),
                              child: DropdownButton<String>(
                                underline: Container(
                                  color:Color.fromRGBO(70, 69, 69, 1) ,
                                ),
                                iconEnabledColor: Colors.white,
                                iconDisabledColor: Colors.white,
                                hint: Padding(
                                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    height: 120,
                                    child: Text(
                                      'Other Payment Opions',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 16,
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                                isExpanded: true,
                                items: [
                                  DropdownMenuItem(
                                    value: "1",
                                    child:  Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              child: Container(
                                                height: 100,
                                                width: 120,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.scaleDown,
                                                    image: AssetImage(
                                                        'assets/images/visaimg.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  DropdownMenuItem(
                                    value: "2",
                                    child: Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              child: Container(
                                                height: 100,
                                                width: 120,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.scaleDown,
                                                    image: AssetImage(
                                                        'assets/images/paypalimg.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),

                                  DropdownMenuItem(
                                    value: "3",
                                    child: Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              child: Container(
                                                height: 120,
                                                width: 150,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.scaleDown,
                                                    image: AssetImage(
                                                        'assets/images/instanteftimg.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),

                                  DropdownMenuItem(
                                    value: "4",
                                    child: Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              child: Container(
                                                height: 120,
                                                width: 150,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.scaleDown,
                                                    image: AssetImage(
                                                        'assets/images/bitcoinimg.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  DropdownMenuItem(
                                    value: "5",
                                    child: Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              child: Container(
                                                height: 120,
                                                width: 150,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.scaleDown,
                                                    image: AssetImage(
                                                        'assets/images/mobicred.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  DropdownMenuItem(
                                    value: "6",
                                    child: Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              child: Container(
                                                height: 120,
                                                width: 80,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.scaleDown,
                                                    image: AssetImage(
                                                        'assets/images/masterpassimg.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),

                                  DropdownMenuItem(
                                    value: "7",
                                    child: Container(
                                      height: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color.fromRGBO(70, 69, 69, 1),
                                              style: BorderStyle.solid)),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              height: 120,
                                              width: 60,
                                              child: CustomRadioBuyFalse()),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              height: 120,
                                              child: Container(
                                                height: 70,
                                                width: 40,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(

                                                    fit: BoxFit.fill,
                                                    image: AssetImage(
                                                        'assets/images/scode.png'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],

                                onChanged: (value) {
                                  updatevalue(value);
                                },
                                value: _value,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                      child: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(70, 69, 69, 1),
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(color: Colors.white)),
                        child: IconButton(
                          icon: Icon(Icons.arrow_forward),
                          iconSize: 22,
                          color: Colors.white,
                          onPressed: () => {
                            Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => PaymentCardDetailPage()),
                            )
                          },
                        ),
                      ),
                    ),
                  ]),
                ))));
  }
  Widget _clueContainer() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: Container(
        height: 160,
        decoration: BoxDecoration(
            border: Border.all(
              color: Color.fromRGBO(70, 69, 69, 1),
            ),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                  child: Text(
                    'June 19, 2019, Friday, 13:52',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontStyle: FontStyle.italic,
                      fontSize: 12,
                      color: Color.fromRGBO(180, 179, 180, 1),
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: IconButton(
                            icon: Image.asset('assets/images/triangle/triangle_units.png'),

                            color: Colors.black,
                            onPressed: () => {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                      child: Container(
                        child: Text(
                          Localization.stLocalized("r50"),
                          style: TextStyle(color: Color.fromRGBO(70, 69, 69, 1),
                              fontSize: 15,
                            fontWeight: FontWeight.w400
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(
                      'Game Units Number:',
                      style: TextStyle(
                          fontSize: 12,
                          color: CColor.appBlackLight,
                          fontWeight: FontWeight.w200),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(
                      'GUWAGenerated6digitNumber',
                      style: TextStyle(
                          fontSize: 12,
                          color: CColor.appBlackLight,
                          fontWeight: FontWeight.w200),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
  Container _paymentSecuredBotomBar() {
    return Container(
      padding: EdgeInsets.only(right: 20),
      color: Color.fromRGBO(180, 179, 180, 1),
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: IconButton(
              icon: Icon(Icons.lock),
              iconSize: 20,
              color: Color.fromRGBO(106, 105, 106, 1),
              onPressed: () => {},
            ),
          ),
          Flexible(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                'Your payment information is secure.',
                textAlign: TextAlign.center,
                style: TextStyle(color: Color.fromRGBO(106, 105, 106, 1)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void updatevalue(String value) {
    setState(() {
      _value = value;
      if( _value == "1")
      {

        imageValue ='assets/images/visaimg.png';
        _value = null;

      }
      else if(_value == "2")
      {
        imageValue ='assets/images/paypalimg.png';
        _value = null;
      }
      else if(_value == "3")
      {
        imageValue ='assets/images/instanteftimg.png';
        _value = null;
      }
      else if(_value == "4")
      {
        imageValue ='assets/images/bitcoinimg.png';
        _value = null;
      }
      else if(_value == "5")
      {
        imageValue ='assets/images/mobicred.png';
        _value = null;
      }
      else if(_value == "6")
      {
        imageValue ='assets/images/masterpassimg.png';
        _value = null;
      }
      else if(_value == "7")
      {
        imageValue ='assets/images/scode.png';
        _value = null;
      }
    });
  }
}
