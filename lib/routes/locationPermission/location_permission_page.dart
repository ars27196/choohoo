import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/routes/main/my_home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LocationPermissionPage extends StatefulWidget {
  @override
  _LocationPermissionPage createState() => _LocationPermissionPage();
}

class _LocationPermissionPage extends State<LocationPermissionPage> {
  bool allowVal = false;
  bool blockVal = true;
  bool wedVal = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
       scrollDirection: Axis.vertical,
        child: Container(
          child: Center(

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 110, 0, 0),
                  child: Container(
                    height: 80,
                    width: 180,

                    decoration: BoxDecoration(

                      image: DecorationImage(
                        image: AssetImage('assets/splash/chohoo_img.png'),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Text("      if yoo find it,\n   it's for yoo hoo   ",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight:FontWeight.w300 )),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
                  child:Container(
                    alignment: Alignment.center,

                  child: Text(
                      "  This game cannot be played without\n  "
                          "accessing the location on your device...",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w200,
                          color: CColor.appYellow,
                          fontSize: 14)),
        ),
                ),

//    Theme(
//      data: ThemeData(unselectedWidgetColor: Colors.yellow,
//        selectedRowColor: Colors.yellow
//      ),
//
//      child:
                Container(
                  alignment: Alignment.center,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: CColor.appYellow,)),
                            child: Checkbox(
                                activeColor: Colors.black,
                                checkColor: CColor.appYellow,
                                value: allowVal,
                                onChanged: (bool value) {
                                  setState(() {
                                    allowVal = value;
                                  });
                                  if (value == true) {
                                    setState(() {
                                      blockVal = false;
                                    });
//                                Navigator.pushReplacement(context, MaterialPageRoute(
//                                    builder: (context) => MyHomePage()),
                                   Navigator.push(
                                     context,
                                      MaterialPageRoute(
                                         builder: (context) => MyHomePage()),
                                   );
                                  } else {
                                    setState(() {
                                      blockVal = true;
                                    });
                                  }
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Text(
                              'Allow',
                              style: TextStyle(
                                color: CColor.appYellow,
                                fontSize: 14,
                                fontFamily: CTheme.defaultFont(),
                                fontWeight: FontWeight.w400
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: CColor.appYellow,)),
                              child: Checkbox(
                                  activeColor: Colors.black,
                                  checkColor:CColor.appYellow,
                                  value: blockVal,
                                  onChanged: (bool value) {
                                    setState(() {
                                      blockVal = value;
                                    });
                                    if (value == true) {
                                      setState(() {
                                        allowVal = false;
                                      });
                                    } else {
                                      setState(() {
                                        allowVal = true;
                                      });
                                    }
                                  }),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Text(
                              'Block',
                              style: TextStyle(
                                color:CColor.appYellow,
                                  fontSize: 14,
                                  fontFamily: CTheme.defaultFont(),
                                  fontWeight: FontWeight.w400
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 50, 0, 30),
                  child: CircularProgressIndicator(
                    backgroundColor: Color.fromRGBO(112, 112, 112, 1),
                    strokeWidth: 5,
                    valueColor: AlwaysStoppedAnimation<Color>(CColor.appYellow,),
                  ),
                ),
//    )

//              Padding(
//                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
//                child: Row(
//                  children: <Widget>[              // [Monday] checkbox
//                    Column(
//                      children: <Widget>[
//                        Text("Mon"),
//                        Checkbox(
//                          checkColor: Colors.yellow,
//                          value: monVal,
//                          onChanged: (bool value) {
//                            setState(() {
//                              monVal = value;
//                            });
//                          },
//                        ),
//                      ],
//                    ),              // [Tuesday] checkbox
//                    Column(
//                      children: <Widget>[
//                        Text("Tu"),
//                        Checkbox(
//                          value: tuVal,
//                          onChanged: (bool value) {
//                            setState(() {
//                              tuVal = value;
//                            });
//                          },
//                        ),
//                      ],
//                    ),              // [Wednesday] checkbox
//                  ],
//                ),
//              ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
