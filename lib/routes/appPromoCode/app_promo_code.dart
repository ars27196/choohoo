import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/utils/AppBarBuy.dart';

class AppPromoCodePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppPromoCodeState();
  }
}

class AppPromoCodeState extends State<AppPromoCodePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBarBuy(),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.only(left: 16, right: 16),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[

              SizedBox(height: 20),
              AutoSizeText(
                Localization.stLocalized("appPromoCode").toUpperCase(),
                style: CTheme.textRegular16DarkGrey(),
                textAlign: Localization.textAlignLeft(),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: <Widget>[
                  Text(
                    Localization.stLocalized("pointsFromCode"),
                    style: CTheme.textRegular12Black(),
                  ),
                  Text(
                    " 20",
                  )
                ],
              ),
              Row(


                children: <Widget>[
                  Text(
                    Localization.stLocalized("pointsFromSpecialCode"),
                    style: CTheme.textRegular12Black(),
                  ),
                  Text(
                    " 50",
                  )
                ],
              ),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    Localization.stLocalized("yourCode").toUpperCase(),
                    style: CTheme.textLight16Black(),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(4,0,10,0),
                      color: CColor.appGreyDark,
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: Text(
                        Localization.stLocalized(
                          "dummyCode",
                        ),
                        style: CTheme.textRegular16White(),
                      ))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  Localization.stLocalized("appPromoMessage"),
                  style: CTheme.textLight16Black(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Center(
                  child: Stack(
                    children: <Widget>[
                      Image.asset("assets/images/share/container.png"),
                      Positioned.fill(
                        child: Align(
                          alignment: Alignment.center,
                          child: Image.asset("assets/images/share/share.png"),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 200,
              )
            ]),
          ),
        ),
      ),
    );
  }
}
