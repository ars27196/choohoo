import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


class PrizeDetailPage extends StatefulWidget {
  @override
  _PrizeDetailPage createState() => _PrizeDetailPage();
}

class _PrizeDetailPage extends State<PrizeDetailPage> {
  bool allowVal = false;
  bool blockVal = true;
  bool wedVal = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[

                Padding(
                  padding: EdgeInsets.fromLTRB(15, 30, 15, 0),
                  child: Container(
                    height: 50,
                    child: Stack(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: Text("PRIZES",
                                  style: TextStyle(
                                      fontFamily: 'Roboto',
                                      color: Color.fromRGBO(180, 179, 180, 1),
                                      fontSize: 16,
                                  )),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.white,
                                  ),
                                  height: 40,
                                  width: 40,
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.black,
                                    ), onPressed: () {
                                      Navigator.pop(context);
                                  },
                                  ),
                                )
                            ),

                          ],
                        )
                      ],
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(15, 30, 15, 0),
                  child: Container(
                    decoration: BoxDecoration(
                      border:Border.all(
                        color: Colors.white,
                        width: 1,
                        style: BorderStyle.solid
                      ) ,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 164,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                           borderRadius: BorderRadius.only(
                             topLeft:Radius.circular(20),
                             topRight: Radius.circular(20)
                           ),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 345,

                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                            child: Text(
                              "1st Prize A: Prize Name",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16
                              ),
                            ),
                          ),
                        ),

                        Container(
                          height: 121,
                          width: 345,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(19),
                              bottomRight: Radius.circular(19)
                            ),
                            color: Colors.grey,
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  child: Text(
                    "Or",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
                  child: Container(
                    decoration: BoxDecoration(
                      border:Border.all(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid
                      ) ,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 164,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft:Radius.circular(20),
                                topRight: Radius.circular(20)
                            ),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 345,

                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                            child: Text(
                              "1st Prize B: Prize Name",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16
                              ),
                            ),
                          ),
                        ),

                        Container(
                          height: 121,
                          width: 345,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(19),
                                bottomRight: Radius.circular(19)
                            ),
                            color: Colors.grey,
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
                  child: Container(
                    decoration: BoxDecoration(
                      border:Border.all(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid
                      ) ,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 164,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft:Radius.circular(20),
                                topRight: Radius.circular(20)
                            ),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 345,

                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                            child: Text(
                              "2nd Prize: Prize Name",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16
                              ),
                            ),
                          ),
                        ),

                        Container(
                          height: 121,
                          width: 345,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(19),
                                bottomRight: Radius.circular(19)
                            ),
                            color: Colors.grey,
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
                  child: Container(
                    decoration: BoxDecoration(
                      border:Border.all(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid
                      ) ,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 164,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft:Radius.circular(20),
                                topRight: Radius.circular(20)
                            ),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 345,

                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                            child: Text(
                              "3rd Prize: Prize Name",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16
                              ),
                            ),
                          ),
                        ),

                        Container(
                          height: 121,
                          width: 345,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(19),
                                bottomRight: Radius.circular(19)
                            ),
                            color: Colors.grey,
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),


                Padding(
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
                  child: Container(
                    decoration: BoxDecoration(
                      border:Border.all(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid
                      ) ,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 164,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft:Radius.circular(20),
                                topRight: Radius.circular(20)
                            ),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 345,

                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                            child: Text(
                              "4th Prize: Prize Name",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16
                              ),
                            ),
                          ),
                        ),

                        Container(
                          height: 121,
                          width: 345,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(19),
                                bottomRight: Radius.circular(19)
                            ),
                            color: Colors.grey,
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 30),
                  child: Container(
                    decoration: BoxDecoration(
                      border:Border.all(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid
                      ) ,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 164,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft:Radius.circular(20),
                                topRight: Radius.circular(20)
                            ),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 345,

                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                            child: Text(
                              "5th Prize: Prize Name",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16
                              ),
                            ),
                          ),
                        ),

                        Container(
                          height: 121,
                          width: 345,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(19),
                                bottomRight: Radius.circular(19)
                            ),
                            color: Colors.grey,
                            image: DecorationImage(
                              image: AssetImage('assets/splash/chohoo_img.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
