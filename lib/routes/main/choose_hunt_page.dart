import 'package:world/constants/CColors.dart';
import 'package:world/routes/digitalWorldHunt/digital_world_hunt_page.dart';
import 'package:world/routes/privateWorldHunt/private_hunts_page.dart';
import 'package:world/routes/realWorldHunt/real_world_hunt_page.dart';
import 'package:world/utils/AppBarMain.dart';
import 'package:world/utils/CustomDrawer.dart';
import 'package:world/utils/HandleNavigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ChooseHuntPage extends StatefulWidget {
  @override
  _ChooseHuntPage createState() => _ChooseHuntPage();
}


class _ChooseHuntPage extends State<ChooseHuntPage> {
  bool allowVal = false;
  bool blockVal = true;
  bool wedVal = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();




  @override
  void initState() {
    super.initState();
    HandleNavigation.appExitCheck = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: AppBarMain(scaffoldKey: _scaffoldKey),
      drawer: CustomDrawer(context),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Container(
                    height: 80,
                    width: 180,

                    decoration: BoxDecoration(

                      image: DecorationImage(
                        image: AssetImage('assets/splash/chohoo_img.png'),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Text("      if yoo find it,\n   it's for yoo hoo   ",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight:FontWeight.w300 )),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
                  child:Container(
                    alignment: Alignment.center,

                    child: Text(
                        "  This game cannot be played without\n  "
                            "accessing the location on your device...",
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Color.fromRGBO(235, 244, 17, 1),
                            fontSize: 14)),
                  ),
                ),

//    Theme(
//      data: ThemeData(unselectedWidgetColor: Colors.yellow,
//        selectedRowColor: Colors.yellow
//      ),
//
//      child:
                Container(
                  alignment: Alignment.center,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: Color.fromRGBO(235, 244, 17, 1),)),
                            child: Checkbox(
                                activeColor: Colors.black,
                                checkColor: Color.fromRGBO(235, 244, 17, 1),

                                value: allowVal,
                                onChanged: (bool value) {
                                  setState(() {
                                    allowVal = value;
                                    if(value)
                                      blockVal=false;
                                    else
                                      blockVal=true;
                                  });

                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Text(
                              'Allow',
                              style: TextStyle(
                                color: CColor.appYellow,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Color.fromRGBO(235, 244, 17, 1),)),
                              child: Checkbox(
                                  activeColor: Colors.black,
                                  checkColor: Color.fromRGBO(235, 244, 17, 1),
                                  value: blockVal,
                                  onChanged: (bool value) {
                                    setState(() {
                                      blockVal = value;
                                    });
                                    if (value == true) {
                                      setState(() {
                                        allowVal = false;
                                      });
                                    } else {
                                      setState(() {
                                        allowVal = true;
                                      });
                                    }
                                  }),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Text(
                              'Block',
                              style: TextStyle(
                                color: Color.fromRGBO(235, 244, 17, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 40, 10, 0),
                  child: Text(
                    "HUNT OPTIONS | CHOOSE & CRACK RIDDLE",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color.fromRGBO(180, 179, 180, 1),
                      fontSize: 16
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                  child: Container(
                    height: 80,
                    width: 205,
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0),
                          side: BorderSide(color: Colors.white)
                      ),
                      color: Color.fromRGBO(70, 69, 69, 1),
                      textColor: Colors.white,
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RealWorldHuntPage()
                          ),
                        );
                      },
                      child: Center(
                        child: Text(
                          "REAL WORLD\n      HUNTS",
                          style: GoogleFonts.roboto(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                  child: Container(
                    height: 80,
                    width: 205,
                    child: FlatButton(

                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0),
                          side: BorderSide(color: Colors.white)),
                      color: Color.fromRGBO(70, 69, 69, 1),
                      textColor: Colors.white,
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DigitalWorldHuntPage()
                          ),
                        );
                      },
                      child: Center(
                        child: Text(
                          "DIGITAL WORLD\n        HUNTS",
                          style: GoogleFonts.roboto(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 40),
                  child: Container(
                    height: 80,
                    width: 205,
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0),
                          side: BorderSide(color: Colors.white)),
                      color: Color.fromRGBO(70, 69, 69, 1),
                      textColor: Colors.white,
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PrivateHuntsPage()),
                        );
                      },
                      child: Text(
                        "PRIVATE\n HUNTS",
                        style: GoogleFonts.roboto(
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
//    )

//              Padding(
//                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
//                child: Row(
//                  children: <Widget>[              // [Monday] checkbox
//                    Column(
//                      children: <Widget>[
//                        Text("Mon"),
//                        Checkbox(
//                          checkColor: Colors.yellow,
//                          value: monVal,
//                          onChanged: (bool value) {
//                            setState(() {
//                              monVal = value;
//                            });
//                          },
//                        ),
//                      ],
//                    ),              // [Tuesday] checkbox
//                    Column(
//                      children: <Widget>[
//                        Text("Tu"),
//                        Checkbox(
//                          value: tuVal,
//                          onChanged: (bool value) {
//                            setState(() {
//                              tuVal = value;
//                            });
//                          },
//                        ),
//                      ],
//                    ),              // [Wednesday] checkbox
//                  ],
//                ),
//              ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
