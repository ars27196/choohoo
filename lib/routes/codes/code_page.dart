import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:world/constants/CColors.dart';
import 'package:world/constants/CTheme.dart';
import 'package:world/constants/localization.dart';
import 'package:world/routes/codes/special_code_detail.dart';
import 'package:world/routes/codes/super_special_code_detail.dart';
import 'package:world/utils/AppBarBuy.dart';
import 'package:world/utils/app_utils.dart';
import 'package:world/utils/enums.dart';

class CodePage extends StatefulWidget {
  final CodeType codeType;

  const CodePage({Key key, this.codeType}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CodePageState();
  }
}

class CodePageState extends State<CodePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBarBuy(),
        body: SingleChildScrollView(
          child: Container(
              width: double.infinity,
              child: Container(
                child: Column(children: <Widget>[
                  SizedBox(height: 20),
                  AutoSizeText(
                    Localization.stLocalized(
                      widget.codeType == CodeType.specialCode ? "specialCode" : "superSpecialCode",
                    ).toUpperCase(),
                    style: CTheme.textRegular16Black(),
                    textAlign: Localization.textAlignLeft(),
                  ),
                  widget.codeType == CodeType.specialCode
                      ? _specialCodeContainer()
                      : _superSpecialContainer(),
                  SizedBox(height: 10),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 20),
                    child: Column(
                      children: <Widget>[
                        Row(children: <Widget>[
                          Expanded(
                              child: Text(
                            Localization.stLocalized(widget.codeType == CodeType.specialCode
                                ? "speicalCodeMessage"
                                : "superSpeicalCodeMessage"),
                            style: CTheme.textLight16Black(),
                          )),
                        ]),
                        _searchBar(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20.0, top: 20),
                          child: ListView(
                            shrinkWrap: true,
                            children: widget.codeType == CodeType.specialCode
                                ? _specialCodeListview()
                                : _superSpecialListview(),
                          ),
                        )
                      ],
                    ),
                  ),
                ]),
              )),
        ));
  }

  _specialCodeContainer() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("specialCodeReceievedDate"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 5"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("specialCodeUsedDate"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 2"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("specialCodeExpires"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 2"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("specialCodeAvailable"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 2"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("specialCodePromo"),
                style: CTheme.textRegular12Black(),
              ),
              Text((" 2")),
            ],
          ),
        ],
      ),
    );
  }

  _superSpecialContainer() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("superSpecialCodeReceievedDate"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 5"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("superSpecialCodeUsedDate"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 2"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("superSpecialCodeExpires"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 2"),
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                Localization.stLocalized("superSpecialCodeAvailable"),
                style: CTheme.textRegular12Black(),
              ),
              Text(" 2"),
            ],
          ),
        ],
      ),
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
      child: Container(
        alignment: Alignment.center,
        height: 50,
        child: TextFormField(
          style: TextStyle(color: Colors.white),
          cursorColor: Colors.white,
          decoration: InputDecoration(
            suffixIcon: IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: () => {}),
            contentPadding: EdgeInsets.fromLTRB(18, 0, 18, 0),
            hintText: 'Search',
            hintStyle: TextStyle(color: Colors.white, fontSize: 16),
            filled: true,
            fillColor: Color.fromRGBO(70, 69, 69, 1),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.white,
                width: 1,
                style: BorderStyle.solid,
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                  width: 1,
                ),
                borderRadius: BorderRadius.all(const Radius.circular(20))),
          ),
        ),
      ),
    );
  }

  _specialCodeListview() {
    return [
      GestureDetector(
        child: Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, left: 15),
                    child: Text(
                      Localization.stLocalized("specialCodeTitle"),
                      style: CTheme.textRegular14Black(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, right: 15),
                    child: Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        border:
                            Border.all(color: Colors.redAccent, width: 1, style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.redAccent,
                      ),
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                    child: Image.asset("assets/images/triangle/triangle_super.png"),
                    decoration: AppUtils.circularBoxDecoration(
                        background: CColor.appWhite, border: CColor.appGreyLight),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0, top: 10),
                      child: AutoSizeText(
                        Localization.stLocalized("codeMessage"),
                        style: CTheme.textLight14Black(),
                      ),
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.start,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      Localization.stLocalized("dateTime"),
                      style: CTheme.textLight11Black(),
                    ),
                  ],
                ),
              )
            ],
          ),
          decoration: AppUtils.circularBoxDecoration(
              background: CColor.appWhite, border: CColor.appGreyLight),
        ),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => SpecialCodeDetail()));
        },
      )
    ];
  }

  _superSpecialListview() {
    return [
      GestureDetector(
        child: Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, left: 15),
                    child: Text(
                      Localization.stLocalized("superSpecialCodeTitle"),
                      style: CTheme.textRegular14Black(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, right: 15),
                    child: Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        border:
                            Border.all(color: Colors.redAccent, width: 1, style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.redAccent,
                      ),
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
              Row(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                      child: ClipRRect(
                        child: Image.asset("assets/images/triangle/triangle_super_special.png"),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          border: Border.all(width: 1, color: Colors.black))),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0, top: 10),
                      child: AutoSizeText(
                        Localization.stLocalized("codeMessage"),
                        style: CTheme.textLight14Black(),
                      ),
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.start,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      Localization.stLocalized("dateTime"),
                      style: CTheme.textLight11Black(),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
              )
            ],
          ),
          decoration: AppUtils.circularBoxDecoration(
              background: CColor.appWhite, border: CColor.appGreyLight),
        ),
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => SuperSpecialCodeDetail()));
        },
      )
    ];
  }
}
