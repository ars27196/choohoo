import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:world/routes/login/login_page.dart';
import 'package:world/routes/signup/signup_name_page.dart';

class AppBarStart extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return  AppBar(
      backgroundColor: Color.fromRGBO(112, 112, 112, 1),
      titleSpacing: 0.0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[

          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.white,
                  onPressed: () => {
                    Navigator.pop(context)
                  },
              ),
//                Positioned(
//                  top: 12.0,
//                  right: 10.0,
//                  width: 10.0,
//                  height: 10.0,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: Colors.black,
//                    ),
//                  ),
//                )
            ],
          ),
        ],
      ),
      automaticallyImplyLeading: false,
      centerTitle: true,
      actions: <Widget>[
        Row(
          children: <Widget>[
            FlatButton(
              color: Color.fromRGBO(112, 112, 112, 1),
              textColor: Colors.white,
              padding: EdgeInsets.all(2.0),
              onPressed: () {

                Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => LoginPage()),);
                debugPrint('Login Clicked');
              },
              child: Text(
                "LOGIN",
                style: GoogleFonts.roboto(
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 8),
              child: FlatButton(
                color: Color.fromRGBO(112, 112, 112, 1),
                textColor: Colors.white,
                padding: EdgeInsets.all(2.0),
                onPressed: () {
                  Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => SignupNamePage()),);
                  debugPrint('Login Clicked');
                },
                child: Text(
                  "SIGN UP",
                  style: GoogleFonts.roboto(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(55);


}