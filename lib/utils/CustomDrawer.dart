import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:world/routes/appPromoCode/app_promo_code.dart';
import 'package:world/routes/bookprivate/book_private.dart';
import 'package:world/routes/buyGameUnits/buy_game_units_page.dart';
import 'package:world/routes/codes/code_page.dart';
import 'package:world/routes/contactUs/contact_us_menu_page.dart';
import 'package:world/routes/howToPlay/how_to_play.dart';
import 'package:world/routes/legal/legal_page.dart';
import 'package:world/routes/myHunts/my_hunts_page.dart';
import 'package:world/routes/notification/notification_page.dart';
import 'package:world/routes/points/my_points_leaderboard_page.dart';
import 'package:world/routes/profile/view_user_profile_page.dart';
import 'package:world/routes/receipts/receipt_page.dart';
import 'package:world/routes/reseller/reseller_page.dart';
import 'package:world/routes/story/our_story.dart';
import 'package:world/routes/technicalUpdate/technical_update.dart';

import 'HandleNavigation.dart';
import 'enums.dart';

class CustomDrawer extends StatelessWidget {
  final BuildContext context;

  CustomDrawer(this.context);

  @override
  Widget build(context) {
    return SafeArea(
      child: Drawer(
        child: Container(
            color: Color.fromRGBO(112, 112, 112, 1),
            child: Stack(
              children: <Widget>[
                ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 20, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          debugPrint("${ModalRoute.of(context).settings.name}");
                          if (ModalRoute.of(context).settings.name ==
                              'MAIN_PAGE') {
                            Navigator.pop(context);
                          } else {
                            Navigator.pop(context);
//                              Navigator.of(context).popUntil(ModalRoute
//                                  .withName('LOCATION_PEMISSION_PAGE'));
//                              Navigator.pushNamed(context, 'MAIN_PAGE');
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                'MAIN_PAGE', (Route<dynamic> route) => false);
                          }
                        },
                        child: Container(
                          width: 100,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Home",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          debugPrint("${ModalRoute.of(context).settings.name}");
                          Navigator.pop(context);

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ViewUserProfilePage()));
                          /* if (ModalRoute.of(context).settings.name == 'MAIN_PAGE') {
                            Navigator.pop(context);
                          }
                          else {
                            Navigator.pop(context);
//                              Navigator.of(context).popUntil(ModalRoute
//                                  .withName('LOCATION_PEMISSION_PAGE'));
//                              Navigator.pushNamed(context, 'MAIN_PAGE');
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                'MAIN_PAGE', (Route<dynamic> route) => false);
                          }*/
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Profile Wallet",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NotificationPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Notifications",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyHuntsPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "My Hunts",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BuyGameUnitsPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Buy or Load Game Units",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CodePage(
                                        codeType: CodeType.specialCode,
                                      )));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Special Codes",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CodePage(
                                        codeType: CodeType.superSpecialCode,
                                      )));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Super Special Codes",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      MyPointsLeaderBoardPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "My Points and Leaderboard",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ReceiptPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Receipts",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ResellerPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Resellers",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BookPrivatePage()
                              ));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Booking & Team Building",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => StoryPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Our Story",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HowToPlayPage()));
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "How To Play, Details & Rules",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ContactUsMenuPage()));

                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Contact Us & FAQ",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TechnicalUpdatePage()));

                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Technical Update",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LegalPage()));

                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Legal v1.01",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 30, 0, 0),
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Time in South Africa: 14:00",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 0, 0),
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Time in Your Country: 14:00",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 30, 0, 35),
                      child: GestureDetector(
                        onTap: () {
                          HandleNavigation.appExitCheck = true;
                          Navigator.pop(context);
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              'LOGIN_PAGE', (Route<dynamic> route) => false);

//                          Navigator.of(context).popUntil(ModalRoute
//                              .withName('LOGIN_PAGE'));
//                          Navigator.pushNamed(context, 'LOGIN_PAGE');

//                          Navigator.pop(context);
//                          Navigator.push(
//                            context,
//                            MaterialPageRoute(
//                                builder: (context) => LoginPage()),
//                          );
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "Logout",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5, 10, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
//                      width: 103,
//                      height: 35,
                        child: FlatButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                              side: BorderSide(color: Colors.white)),
                          color: Color.fromRGBO(112, 112, 112, 1),
                          textColor: Colors.white,
                          padding: EdgeInsets.all(6.0),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AppPromoCodePage()));

                          },
                          child: Text(
                            "  App Promo Code  ",
                            style: GoogleFonts.roboto(
                              fontSize: 11,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
