import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppBarBuy extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return  AppBar(
      backgroundColor: Color.fromRGBO(70, 69, 69, 1),
      titleSpacing: 0.0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[

          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.white,
                onPressed: () => {
                  Navigator.pop(context)
                },
              ),
//                Positioned(
//                  top: 12.0,
//                  right: 10.0,
//                  width: 10.0,
//                  height: 10.0,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: Colors.black,
//                    ),
//                  ),
//                )
            ],
          ),
      IconButton(
        icon: Image.asset('assets/images/chohooappbar.png'),
        iconSize: 55,
        onPressed: () => debugPrint('Back icon clicked'),
         )
        ],
      ),
      automaticallyImplyLeading: false,
      centerTitle: true,
      actions: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
              child: FlatButton(
                color: Color.fromRGBO(70, 69, 69, 1),
                textColor: Colors.white,
                padding: EdgeInsets.all(2.0),
                onPressed: () {
                  debugPrint('Name Nick Name Clicked');
                },
                child: Text(
                  "Heya Name/NickName",
                  style: GoogleFonts.roboto(
                    fontSize: 12,
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(55);


}