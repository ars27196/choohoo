import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:world/constants/CTheme.dart';

class AppBarMain extends StatelessWidget implements PreferredSizeWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  AppBarMain({@required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Color.fromRGBO(112, 112, 112, 1),
      titleSpacing: 0.0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.menu),
            iconSize: 40,
            color: Colors.white,
            onPressed: () => scaffoldKey.currentState.openDrawer(),
          ),
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.white,
                onPressed: () {
                  debugPrint("${ModalRoute.of(context).settings.name}");
                  if (ModalRoute.of(context).settings.name == 'MAIN_PAGE') {
                    CTheme.showAppAlert(
                      context: context,
                      title: "Chohoo",
                      bodyText: "Do you want to Close the Application?",
                      btnTitle: "Okay",
                      negativeButtonHandler: (_){
                        Navigator.pop(context);
                      },
                      positiveButtonHandler: (_){
                        Future.delayed(const Duration(milliseconds: 1000), () {
                          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                        });
                      }
                    );
                  } else {
                    Navigator.pop(context);
                  }
                },
              ),
//                Positioned(
//                  top: 12.0,
//                  right: 10.0,
//                  width: 10.0,
//                  height: 10.0,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: Colors.black,
//                    ),
//                  ),
//                )
            ],
          ),
        ],
      ),
      automaticallyImplyLeading: false,
      centerTitle: true,
      actions: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
              child: FlatButton(
                color: Color.fromRGBO(112, 112, 112, 1),
                textColor: Colors.white,
                padding: EdgeInsets.all(2.0),
                onPressed: () {
                  debugPrint('Name Nick Name Clicked');
                },
                child: Text("Heya Name/NickName",
                    style: CTheme.textRegular14White()),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(55);
}
