import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBarSignup extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
        return  AppBar(
      backgroundColor: Color.fromRGBO(112, 112, 112, 1),
      titleSpacing: 0.0,
      title: Stack(
        children: <Widget>[
          Container(
            height: 55,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      color: Colors.white,
                      onPressed: () => {
                        Navigator.pop(context)
                      },
                    ),
//                Positioned(
//                  top: 12.0,
//                  right: 10.0,
//                  width: 10.0,
//                  height: 10.0,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: Colors.black,
//                    ),
//                  ),
//                )
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 55,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  height: 55,
                  child: Text(
                    'SIGN UP',
                    style: TextStyle(fontFamily: 'Roboto', fontSize: 20),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      automaticallyImplyLeading: false,
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(55);


}